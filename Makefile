install:
	virtualenv -p /usr/bin/python3 venv
	venv/bin/python setup.py install
	npm install
	venv/bin/python manage.py migrate --noinput


develop:
	parallelshell "live-reload --port 9091 farmdrop/' 'watch 'npm run build:css'"
	

deps:
	sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk libxslt-dev libxml2-dev
	npm install bower
	

deps_mac:
	brew install libtiff libjpeg webp little-cms2


test:
	rm -rf .tox
	detox

clean:
	rm -rf venv

run:
	venv/bin/python manage.py runserver_plus 0.0.0.0:45000

rename:
	find . -maxdepth 1 -type f \( ! -iname "Makefile" \) -print0 | xargs -0 sed -i 's/mljtrust/$(name)/g'
	find mljtrust -maxdepth 1 -type f -print0 | xargs -0 sed -i 's/mljtrust/$(name)/g'
	mv mljtrust/manage_mljtrust.py mljtrust/manage_$(name).py
	mv mljtrust $(name)
	echo "Great, you're all set! Well, you'll probably want to adjust the setup file by hand a bit."

tag-release:
	sed -i "/__version__/c\__version__ = '$(v)'" mljtrust/__init__.py
	git add mljtrust/__init__.py && git commit -m "Automated version bump to $(v)" && git push
	git tag -a release/$(v) -m "Automated release of $(v) via Makefile" && git push origin --tags

package:
	rm -rf build
	python setup.py clean
	python setup.py build sdist bdist_wheel

distribute:
	twine upload -r fiveq -s dist/mljtrust-$(v)*

release:
	$(MAKE) tag-release
	$(MAKE) package
	$(MAKE) distribute

