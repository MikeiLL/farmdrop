from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import sys

version = __import__('farmdrop').__version__

install_requires = [
    'Django>=1.9,<1.10',
    'django_configurations>=1.0',
    'dj-database-url>=0.3.0',
    'dj-haystack-url>=0.2.0',
    'django-cache-url>=0.8.0',
    'pylibmc>=1.5.0',
    'werkzeug>=0.9.4',
    'Pillow>=3',
    'whoosh>=2.7.4',
    'Unidecode>=0.04', # for filer
    'gunicorn>=0.17.4',
    'boto==2.39.0',
    'markdown==2.6.1',
    'easy-thumbnails>=1.2',
    'whitenoise>=3.2',
    'satchless>=1.1.3',
    'google-measurement-protocol>=0.1.6',
    'google-i18n-address>=1.0.7',
    'jsonfield>=1.0.3',
    'maxminddb-geolite2>=2015.708',
    'django-jenkins>=0.18',
    'django-haystack>=2.4',
    'django-debug-toolbar>=1.4',
    'django-extensions>=1.6.1',
    'django-braces>=1.4.0',
    'django-localflavor>=1.1',
    'django-allauth>=0.24.1',
    'django-floppyforms>=1.6.1',
    'django-custom-user>=0.5',
    'django-nose>=1.4.1',
    'django-smartcc>=0.1.2',
    'django-storages>=1.4,<2',
    'djangorestframework==3.3.2',
    'django-cors-headers==1.1.0',
    'django-templated-email==0.4.9',
    'django-robots==2.0',
    'django-typogrify==1.3.3',
    'django-absolute>=0.3',
    'django-bootstrap3>=7.0.1',
    'django-countries>=3.4.1',
    'django-emailit>=0.2.2',
    'django-materializecss-form>=1.0.1',
    'django-model-utils>=2.5',
    'django-mptt>=0.8.4',
    'django-offsite-storage>=0.0.10',
    'django-payments>=0.9.4',
    'django-prices>=0.4.13',
    'django-redis>=4.4.3',
    'django-selectable>=0.9.0',
    'django-versatileimagefield>=1.4',
    'raven>=5.2.0',
    'psycopg2>=2.5',
]


class Tox(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import tox
        errno = tox.cmdline(self.test_args)
        sys.exit(errno)

setup(
    name="farmdrop",
    version=version,
    url='http://gitlab.com/farmdrop/farmdrop',
    license='Closed',
    platforms=['OS Independent'],
    description="MLJtrust.org Django applications.",
    author="Colin Powell",
    author_email='colin.powell@fiveq.com',
    packages=find_packages(),
    install_requires=install_requires,
    include_package_data=True,
    zip_safe=False,
    tests_require=['tox'],
    cmdclass={'test': Tox},
    classifiers=[
        'Development Status :: 4 - Beta',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    package_dir={
        'farmdrop': 'farmdrop',
        'farmdrop/templates': 'farmdrop/templates',
    },
    entry_points={
        'console_scripts': [
            'farmdrop = farmdrop.manage_farmdrop:main',
        ],
    },
)
