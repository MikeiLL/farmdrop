Farmdrop Multi-vendor Shop
===========================

Built on the [Saleor](http://getsaleor.com/) Python Storefront. 
---------------------------------------------------------------

# Easy bootstrapping! #

Powered by the ubiquitous Makefile ... this should be pretty easy:

1. make deps or make deps_mac
2. make install
3. make run
4. open your browser to: http://127.0.0.1:45000

Now go back and edit the file `farmdrop/settings.py`, adding your name and email to the ADMINS list.

From within the "inner" farmdrop directory (_within_ the repo), run `python manage_farmdrop.py createsuperuser` to create your user account.
If you get and ImportErrors, try manually running the command `pip install -r requirements.txt` from the top level of the repository (the _outer_ farmdrop directory).

# NOTE: You may need to install: #

 * [Node.js](https://docs.npmjs.com/getting-started/installing-node)
 * `zlib` for Pillow to work: [instructions on SO](http://stackoverflow.com/a/34631976/2223106)
 * `libmemcached` which will also be installed/managed via `yum`, `apt-get`, `brew`, `macports`, `fink`, etc depending on your OS.
 	- for OS errors here you may have to `export` [CPPFLAGS and LDFLAGS](http://stackoverflow.com/a/19432949/2223106)
 * For Apple, XCode Command Line Tools via `xcode-select --install` or download from [Apple Developer Site](https://developer.apple.com/downloads/)
 * [Grunt.js](http://stackoverflow.com/questions/15703598/how-to-install-grunt-and-how-to-built-script-with-it)
  - then run `$ grunt` in command line to generate the site assets
 * Celery via `pip install celery`
 